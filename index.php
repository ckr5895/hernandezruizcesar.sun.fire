<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Login</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/main.css">
<script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body background="img/biblioteca.png">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<center><IMG SRC="img/logo.jpg" align="center" WIDTH=178 HEIGHT=180 style=margin-top:2%></center>
<div class="container" style=margin-top:3%>
      <form class="form" role="form" method="POST" action="sesion/inicio_sesion.php">
		<label>u=bibliotecario p=bibliotecario</label>
		<br><br>
        <input type="text" class="form-control" placeholder="Usuario" name="u">
		<br>
        <input type="password" class="form-control" placeholder="Contraseña" name="c">
		<br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
      </form>
    </div> <!-- /container -->



<script src="js/vendor/jquery-1.11.0.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>