<?php foreach ($datos['libros'] as $libro) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		libro: <strong><?php echo $libro['titulo_libro']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
					<li><strong>ID:</strong> <?php echo $libro['id_libro']; ?></li>
					<li><strong>ISBN:</strong> <?php echo $libro['isbn_libro']; ?></li>
					<li><strong>Editorial:</strong> <?php echo $libro['editorial_libro']; ?></li>
					<li><strong>Año de publicación:</strong> <?php echo $libro['anio_publicacion_libro']; ?></li>
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="inicio_biblioteca.php?c=libros&a=editar_libro&id_libro=<?php echo $libro['id_libro']; ?>" class="btn btn-default">Editar</a>
                        <a href="inicio_biblioteca.php?c=ejemplares&a=ver_ejemplares&v=panel&isbn=<?php echo $libro['isbn_libro']; ?>" class="btn btn-success">Ver Ejemplares</a>
			<a href="inicio_biblioteca.php?c=libros&a=borrar_libro&id_libro=<?php echo $libro['id_libro']; ?>" class="btn btn-warning">Borrar</a>
		</div>
	</div>
</div>
<?php } ?>