<div class="row">
	<div class="col-md-2">
		<a href="inicio_biblioteca.php?c=libros&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="inicio_biblioteca.php?c=libros&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Libros</caption>
			<thead>
				<tr>
					<th>Libro</th>
					<th>Editorial </th>
					<th>Año</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['libros'] as $libro) { ?>
    <tr>
					<td><?php echo $libro['titulo_libro'];?></td>
					<td><?php echo $libro['editorial_libro']; ?></td>
					<td><?php echo $libro['anio_publicacion_libro']; ?></td>
					<td><a
						href="inicio_biblioteca.php?c=libros&a=ver_libro&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-primary btn-xs">Información</a> <a
						href="inicio_biblioteca.php?c=libros&a=editar_libro&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-default btn-xs">Editar</a> <a
                        href="inicio_biblioteca.php?c=ejemplares&a=ver_ejemplares&v=tabla&isbn=<?php echo $libro['isbn_libro']; ?>"
						class="btn btn-success btn-xs">Ver ejemplares</a> <a
						href="inicio_biblioteca.php?c=libros&a=borrar_libro&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-warning btn-xs">Borrar</a></td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>