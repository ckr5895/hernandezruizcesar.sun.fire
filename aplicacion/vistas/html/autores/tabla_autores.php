<div class="row">
	<div class="col-md-2">
		<a href="inicio_b.php?c=autores&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="inicio_b.php?c=autores&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Autores</caption>
			<thead>
				<tr>
					<th>ID</th>
					<th>Autor</th>
					<th>Nacionalidad</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['autores'] as $autor) { ?>
    <tr>
					<td><?php echo $autor['id_autor']; ?></td>
					<td><?php echo $autor['nombre_autor']; ?></td>
					<td><?php echo $autor['nacionalidad_autor']; ?></td>
					<td><a 
						href="inicio_biblioteca.php?c=autores&a=ver_autor&id_autor=<?php echo $autor['id_autor']; ?>"
						class="btn btn-primary btn-xs">Información</a> <a
						href="inicio_biblioteca.php?c=libros&a=ver_libro_autor&v=tabla&id_autor=<?php echo $autor['id_autor']; ?>"
						class="btn btn-success btn-xs">Ver libros</a> <a
						href="inicio_biblioteca.php?c=autores&a=editar_autor&id_autor=<?php echo $autor['id_autor']; ?>"
						class="btn btn-default btn-xs">Editar</a> <a
						href="inicio_biblioteca.php?c=autores&a=borrar_autor&id_autor=<?php echo $autor['id_autor']; ?>"
						class="btn btn-warning btn-xs">Borrar</a></td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>