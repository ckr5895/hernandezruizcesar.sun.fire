<?php foreach ($datos['autores'] as $autor) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		Autor: <strong><?php echo $autor['nombre_autor']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>Nacionalidad:</strong> <?php echo $autor['nacionalidad_autor']; ?></li>
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="inicio_b.php?c=libros&a=ver_libro_autor&v=tabla&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-success">Ver libros</a>
			<a href="inicio_b.php?c=autores&a=editar_autor&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-default">Editar</a>
			<a href="inicio_b.php?c=autores&a=borrar_autor&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-warning">Borrar</a>
		</div>
	</div>
</div>
<?php } ?>