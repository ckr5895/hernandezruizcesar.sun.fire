<?php

function obtener_conexion_base_datos()
{
    /* Datos para el acceso a PostgreSQL */
    $servidor = '127.0.0.1';
    $puerto = 5432;
    $usuario = 'postgres';
    $password = 'gudu5895';
    $base_datos = 'biblioteca';
    
    /* "Intentamos" obtener acceso a PostgreSQL */
    $conexion = pg_connect("
        host=$servidor
        port=$puerto
        user=$usuario
        password=$password
        dbname=$base_datos");
    
    /*
     * Si no obtenemos acceso a PostgreSQL, regresamos un mensaje de error.
     */
    if ($conexion == false) {
        return array(
            'error' => true,
            'mensajes_error' => array('No se tiene acceso a la base de datos.')
        );
    }
    
    /* En caso contrario, regresamos la *conexión* */
    return array(
        'error' => false,
        'conexion' => $conexion
    );
}

function cerrar_conexion_base_datos($conexion)
{
    pg_close($conexion);
}